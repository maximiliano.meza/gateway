/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.gateway.agent.utils;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.secureflag.gateway.agent.model.Constants;

public class GatewayAgentConfig {
	
	private static Properties config = new Properties();
	private static String agentUser = "";
	private static String agentPassword = "";

	private static Logger logger = LoggerFactory.getLogger(GatewayAgentConfig.class);

	static{
		try {
			config.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(Constants.CONFIG_FILE));
			GatewayAgentConfig.agentUser = config.getProperty(Constants.AGENT_USER);
			GatewayAgentConfig.agentPassword = config.getProperty(Constants.AGENT_PASSWORD);
			logger.info("GatewayAgent configuration parsed");
		} catch (IOException e) {
			logger.error(e.toString());
		}			
	}
	
	public static String getAgentUsername() {
		return agentUser;
	}
	public static String getAgentPassword() {
		return agentPassword;
	}
}