/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.gateway.agent.actions;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.gateway.agent.messages.MessageGenerator;
import com.secureflag.gateway.agent.model.Constants;
import com.secureflag.gateway.agent.model.FileResponse;
import com.secureflag.gateway.agent.utils.HTTPUtils;

public class GetResultZipAction extends IAction{

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		JsonObject actionObject = new JsonObject();
		actionObject.addProperty(Constants.JSON_ATTRIBUTE_ACTION, Constants.ACTION_GET_RESULT_ARCHIVE);

		String diffAction = actionObject.toString();

		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement exerciseIpElement = json.get(Constants.INSTANCE_IP);
		if(null==exerciseIpElement){
			MessageGenerator.sendErrorMessage("Missing IP", response);
			logger.warn("Received null IP when requesting zip");
			return;
		}
		JsonElement baseNameElement = json.get(Constants.BASENAME);

		FileResponse diffFileResponse = HTTPUtils.sendGetFileArchiveRequest(exerciseIpElement.getAsString(), diffAction);

		if(null==diffFileResponse || null==diffFileResponse.getContentDisposition() || null==diffFileResponse.getContentType() || null==diffFileResponse.getFileContent()){
			MessageGenerator.sendErrorMessage(Constants.ERROR_COULD_NOT_FETCH, response);
			logger.warn("Could not fetch results archive from "+exerciseIpElement.getAsString());
			return;
		}
		try {
			response.setContentType(diffFileResponse.getContentType());
			response.setHeader(Constants.CONTENT_DISPOSITION_HEADER, diffFileResponse.getContentDisposition());
			ServletOutputStream out = response.getOutputStream();
			out.write(diffFileResponse.getFileContent());
			out.flush();

			if(null!=baseNameElement && null!=baseNameElement.getAsString()) {
				
				(new Thread(new Runnable(){
					   public void run(){
						   try {
								String diffFileName = "/tmp/"+baseNameElement.getAsString()+"-diff.zip";
								Path diffPath = Paths.get(diffFileName);
								Files.write(diffPath, diffFileResponse.getFileContent());
								HTTPUtils.sendGet(Constants.UPLOADER_URL+"?path="+diffFileName);
							}catch(Exception e) {
								logger.error("Diff for "+baseNameElement.getAsString()+" could not be uploaded to S3 due to: "+e.getMessage());
							}
							JsonObject logsActionObject = new JsonObject();
							logsActionObject.addProperty(Constants.JSON_ATTRIBUTE_ACTION, Constants.ACTION_GET_LOGS_ARCHIVE);
							String logsAction = logsActionObject.toString();
							FileResponse logsFileResponse = HTTPUtils.sendGetFileArchiveRequest(exerciseIpElement.getAsString(), logsAction);
							if(null!=logsFileResponse) {
								try {
									String logsFilePath = "/tmp/"+baseNameElement.getAsString()+"-logs.zip";
									Path logsPath = Paths.get(logsFilePath);
									Files.write(logsPath, logsFileResponse.getFileContent());
									HTTPUtils.sendGet(Constants.UPLOADER_URL+"?path="+logsFilePath);
								}catch(Exception e) {
									logger.error("Logs for "+baseNameElement.getAsString()+" could not be uploaded to S3 due to: "+e.getMessage());
								}
							}
							String videoFilePath = "/tmp/videos/"+baseNameElement.getAsString();
							File videoFile = new File(videoFilePath);
							if(videoFile.exists()) {
								try{ 
									HTTPUtils.sendGet(Constants.UPLOADER_URL+"?path="+videoFilePath);
								}catch(Exception e) {
									logger.warn("Video for "+baseNameElement.getAsString()+" could not be uploaded to S3 due to "+e.getMessage());
								}
							}
							else {
								logger.warn("Video for "+baseNameElement.getAsString()+" could not be uploaded to S3 as it does not exist");
							}
					   }
					})).start();
			}
		} catch (Exception e) {
			MessageGenerator.sendErrorMessage(Constants.ERROR_COULD_NOT_FETCH, response);
			logger.error(e.getMessage());
		}
	}
}