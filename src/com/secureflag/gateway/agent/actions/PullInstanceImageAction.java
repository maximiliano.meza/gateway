/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.gateway.agent.actions;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.gateway.agent.messages.MessageGenerator;
import com.secureflag.gateway.agent.model.Constants;
import com.secureflag.gateway.agent.model.ExerciseImage;
import com.secureflag.gateway.agent.utils.HTTPUtils;

public class PullInstanceImageAction extends IAction{

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {
		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement instanceIpElement = json.get(Constants.INSTANCE_IP);
		JsonElement imageUrlElement = json.get(Constants.IMAGE_URL);
		if(null==instanceIpElement || null==imageUrlElement){
			MessageGenerator.sendErrorMessage("Missing IP/URL", response);
			logger.warn("Received null string when pulling from instance");
			return;
		}
		ExerciseImage image = new ExerciseImage();
		image.setUrl(imageUrlElement.getAsString());
		Gson gson = new GsonBuilder().create();
		String body = gson.toJson(image);
		
		String responseString = HTTPUtils.sendPost("http://"+instanceIpElement.getAsString()+Constants.URL_PULL_INSTANCE_IMAGE,body);
		if(null==responseString){
			MessageGenerator.sendErrorMessage(Constants.ERROR_COULD_NOT_FETCH, response);
			logger.warn("Received null string when pulling from instance "+instanceIpElement.getAsString()+" for image: "+image.getUrl());
			return;
		}
		try {
			response.setContentType(Constants.CONTENT_TYPE_JSON);
			PrintWriter out = response.getWriter();
			out.print(responseString);
			out.flush();
		} catch (Exception e) {
			logger.error(e.getMessage());
			MessageGenerator.sendErrorMessage(Constants.ERROR_COULD_NOT_FETCH, response);
		}
	}
}