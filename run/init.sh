# 
# This file is part of the SecureFlag Platform.
# Copyright (c) 2020 SecureFlag Limited.
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

echo "Starting SecureFlag Gateway CE Service..."

if [ -z "$ELB" ]; then
	echo 'Skipping ELB Configuration...'
	mv /tmp/nginx/notls /etc/nginx/sites-available/default
	/etc/init.d/nginx start
else
	echo 'Performing ELB Configuration...'
	sed -i "s/FQDNTOREPLACE/${DOMAIN}/g" /tmp/nginx/elb
	sed -i "s/PORTALFQDN/${PORTAL_DOMAIN}/g" /tmp/nginx/elb
	mv /tmp/nginx/elb /etc/nginx/sites-available/default
	/etc/init.d/nginx start
fi

echo "mysql-password: $MYSQL_GUAC_PASSWORD" >> /var/lib/tomcat8/.guacamole/guacamole.properties
echo "mysql-hostname: mysqlhost" >> /var/lib/tomcat8/.guacamole/guacamole.properties

sed -i "s/GATEWAY_AGENT_PASSWORD/${GATEWAY_AGENT_PASSWORD}/g" /var/lib/tomcat8/webapps/helper/WEB-INF/classes/config.properties

/etc/init.d/guacd start
mkdir -p /usr/share/tomcat8/logs
/etc/init.d/tomcat8 start
rm /tmp/init.sh
rm -rf /tmp/nginx
tail -F /var/lib/tomcat8/logs/catalina.out

