# 
# This file is part of the SecureFlag Platform.
# Copyright (c) 2020 SecureFlag Limited.
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

FROM ubuntu:20.04
ARG DEBIAN_FRONTEND=noninteractive

RUN echo "deb http://security.ubuntu.com/ubuntu bionic-security universe" >> /etc/apt/sources.list \
&& echo "# deb-src http://security.ubuntu.com/ubuntu bionic-security universe" >> /etc/apt/sources.list \
&& apt-get update && apt-get -y upgrade && apt-get -y install \ 
    libcairo2-dev libpng-dev libossp-uuid-dev libavformat-dev libavcodec-dev libavutil-dev libswscale-dev \ 
    freerdp2-dev libpango1.0-dev libssh2-1-dev libssl-dev libvorbis-dev \ 
    libwebp-dev nginx wget libjpeg-turbo8-dev libwebsockets-dev autoconf libtool pkg-config gcc g++ make unzip \
    && mkdir -p /build-box/ \ 
    && cd /build-box \
    && wget -q https://mirrors.gethosted.online/apache/guacamole/1.2.0/source/guacamole-server-1.2.0.tar.gz \ 
    && tar -zxf guacamole-server-1.2.0.tar.gz \
    && cd guacamole-server-1.2.0 \  
    && ./configure --with-init-dir=/etc/init.d \ 
    && make \ 
    && make install \ 
    && update-rc.d guacd defaults \ 
    && ldconfig \ 
    && rm -Rf /tmp/*  \ 
    && apt-get clean && apt-get -y remove && apt-get -y autoremove \ 
    && apt-get update && apt-get -y install openjdk-8-jdk tomcat8

COPY    gateway.zip /tmp/gateway.zip
RUN     unzip /tmp/gateway.zip -d /tmp/ \
        && rm -rf /var/lib/tomcat8/webapps \ 
        && mv /tmp/fs/webapps /var/lib/tomcat8/ \ 
        && cp -Rf /tmp/fs/guacamole_home/. /var/lib/tomcat8/.guacamole/ \ 
        && mv /tmp/run/nginx /tmp/nginx \ 
        && mv /tmp/run/init.sh /tmp/init.sh \ 
        && rm -rf /tmp/fs && rm -rf /tmp/run \ 
        && rm /tmp/gateway.zip \
        && chown -R tomcat8:tomcat8 /var/lib/tomcat8/ \
        && chmod +x /tmp/init.sh

EXPOSE 80
ENTRYPOINT /tmp/init.sh